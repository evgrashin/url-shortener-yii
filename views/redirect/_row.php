<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Redirect */
?>

<tr class="redirect-row">
    <td>
        <?= Html::a($model->url, $model->url, ['class' => 'link']) ?>
    </td>
    <td>
        <?= Html::a(Url::base(true).'/'.$model->code, 
                    Url::base(true).'/'.$model->code, 
                    ['class' => 'link', 'target' => '_blank']) ?>

        <?= Html::a('<i class="fa fa-copy"></i>', 'javascript:;', 
                    ['id' => $model->code, 
                    'class' => 'pseudo js-copy', 
                    'data-clipboard-text' => Url::base(true).'/'.$model->code, 
                    'target' => '_blank']) ?>
    </td>
    <td>
        <?= Html::tag('span', Html::encode($model->created_at), ['class' => 'date']) ?>
    </td>
</tr>
