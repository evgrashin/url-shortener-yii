<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Yii ULR Shortener';
?>
<div class="site-index my-2">

    <div class="jumbotron">
        <h2>Укротитель ссылок</h2>

        <?php $form = ActiveForm::begin(['options' => ['id'=>'main-form']]); ?>

        <?= HTML::tag('h3', 'Вставьте ссылку или введите ее начиная с «http://»', ['class' => 'text-secondary small']) ?>

            <?= $form->field($model, 'url')->label(false)->textInput()->input('url', [ 
                'id'=>'origin-url',
                'placeholder' => 'Например, http://yourwebsite.com/any/path/',
                //'value' => 'http://test.org'
                ]);
                ?>

            <div class="form-group">
                <?= Html::submitButton('Укоротить', [
                    'id'=>'main-form__button',
                    'class' => 'btn btn-primary'])
                    ?>
            </div>

            <input id="short-url" type="url" class="form-control hidden" require max="255" >

        <?php ActiveForm::end(); ?>

        <table id="history" class="table">
            <tr class="hidden"><th>Оригинальная сслыка</th><th>Укороченная ссылка</th><th>Дата создания</th></tr>
        </table>
    </div>

    <div class="body-content">
    </div>

</div>
