<?php

namespace app\controllers;

use Yii;
use app\models\Redirect;
use app\models\RedirectSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * RedirectController implements the CRUD actions for Redirect model.
 */
class RedirectController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Redirect models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RedirectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Redirect model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Redirect model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Redirect();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->code]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Redirect model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->code]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Redirect model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Redirect model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Redirect the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Redirect::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Переходит по URL, который соответствует полученному коду.
     * Либо возвращает ошибку.
     */
    public function actionGoToUrl($code)
    {        
        if (($model = $this->findModel($code)) !== null) {
            return Yii::$app->response->redirect($model['url']);
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Генерирует короткий ULR, создает запись о его соответствии оригинальному URL.
     * Возвращает JSON, содержащий короткий URL и HTML для отображения в клиенте.
     */
    public function actionGenerateUrl()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();

            if (($model = Redirect::find()->where([ 'url' => $data['url'] ])->one()) !== null){
                $response = [
                    'short_url' => $this->convertToUrl($model['code']),
                    'html' => $this->renderPartial('_row', [
                        'model' => $model])
                ];

                return $response;
            }

            $model = new Redirect();
            $model['url'] = $data['url'];
            $model['code'] = $this->generateCode();
            $model['created_at'] = date("Y-m-d H:i:s");
            
            if ($model->save()) {
                $response = [
                    'short_url' => $this->convertToUrl($model['code']),
                    'html' => $this->renderPartial('_row', [
                        'model' => $model])
                ];

                return $response;
            }

            return ['code' => '400'];
        }
    }

    /**
     * Генерирует случайный код для идентификации ссылки.
     */
    public function generateCode()
    {
        $min = 5;
        $max = 10;
        $length = rand($min , $max);

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    public function convertToUrl($code)
    {
        return Url::base(true).'/'.$code;
    }
}
