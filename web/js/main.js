$(document).ready(function (){
    /**
     * При переходе по сгенерированной ссылке - перенаправление на оригинальный сайт
     */
    if(document.location.pathname != '/'
    && document.location.pathname != '/redirect'
    && window.location.search == ''
    ){        
        let path = '/redirect/go-to-url';
        let search = '?code=' + document.location.pathname.slice(1);

        window.location.replace(window.location.protocol + "//" + window.location.host + path + search);
    }

    /**
     * Фоновый запрос на генерацию короткой ссылки
     */
    $('#main-form').on('beforeSubmit', function(){
        $("#main-form__button").prop("disabled", true);
        let url = $('input[type="url"]').val();
     
        $.ajax({
            url: "/redirect/generate-url",
            type: "post",
            data: {
                _csrf: yii.getCsrfToken(),
                url: url,
            },
            success: function(data){
                console.log("200");
                console.log(data['short_url']);
                
                $("#short-url").val(data['short_url']);
                $("table").prepend(data['html']);
                copyToClipboard('#short-url');
            },
            error: function(error){
                console.log( "400");
                console.log( error);
            }
        }).done(function() {
            $("#main-form__button").prop("disabled", false);
        });
    
        return false;
    });
    
    /**
     * Обработка кнопок для копирования ссылок в буфер 
     */
    $("#history").on("click", ".js-copy", function(){
        let buf = $(this).data('clipboard-text');
        console.log(buf);
        copyToClipboard(buf);
        let id = $(this).attr('id');
        $('.js-copied-'+id).show().css('opacity', 1.0).clearQueue().fadeOut(3000);
    });
});


const copyToClipboard = str => {
    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
};