<?php

namespace app\models;

use yii\base\Model;

class MainForm extends Model
{
    public $url;

    public function rules()
    {
        return [
            ['url', 'required', 'message' => 'Пожалуйста, введите URL'],
            ['url', 'url'],
        ];
    }
}