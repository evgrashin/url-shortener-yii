<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "redirect".
 *
 * @property string $code
 * @property string $url
 * @property string $created_at
 */
class Redirect extends \yii\db\ActiveRecord
{
    private $_redirect = false;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'redirect';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'url'], 'required'],
            [['created_at'], 'safe'],
            [['code'], 'string', 'max' => 10],
            [['url'], 'string', 'max' => 255],
            [['url'], 'url', 'defaultScheme' => 'http'],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'code' => 'Code',
            'url' => 'Url',
            'created_at' => 'Created At',
        ];
    }
}
