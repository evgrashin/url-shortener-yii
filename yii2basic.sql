CREATE TABLE IF NOT EXISTS `redirect` (
  `code` VARCHAR(10) NOT NULL,
  `url` VARCHAR(255) NOT NULL,
  `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY(`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `redirect` VALUES ('ya', 'yandex.ru', NOW());
INSERT INTO `redirect` VALUES ('vk', 'vk.com/o.evgrashin', NOW());